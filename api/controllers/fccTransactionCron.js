'use strict';

const cron = require('node-cron');
const yargs = require('yargs');
const request = require('request');
const k8sDriven = require('../config/k8sDrivenConfig.js');
const moment = require('moment');
const requestRetry = require('requestretry');
const log4js = require('log4js');
const logger = log4js.getLogger('logs'); // to log errors in a log file

const oracleDb = require('./oracleDb.js');
var data;

module.exports = {
	startCron
}

var msgId = new Date().getTime();

function startCron() {
	var configDetails = require('../config/configUrl.js');
	data = {
		headers: {
			'Content-Type' : 'application/json'
		},
		config: configDetails.config,
		maxAttempts: process.env.RETRY_ATTEMPTS?parseInt(process.env.RETRY_ATTEMPTS):5,
        retryDelay: process.env.RETRY_DELAY?parseInt(process.env.RETRY_DELAY):1000
	};

	//console.info("MsgId: ",msgId, ";FCC Transaction Cron: ",  "request received: " + new Date());
    logger.info("MsgId: ",msgId, "; EOD FCC Transaction Cron: ",  "request received: " + new Date());
	
	getToken(data)
		.then(getSchedule)
		.then(updateFlag)
		.catch((error) => {
			console.log(error);
		});
}

function getToken(data) {
	return new Promise((resolve, reject) => {
	    var body = {
	    	"username": k8sDriven.omniDataPlatformUser,
	    	"password": k8sDriven.omniDataPlatformPassword,
	    	"encrypted": true
	    };
	    //console.log(data.config.lrsApiUrl+'/api/lrs/login');
	    request(
		{
		  	headers: data.headers,      
		  	url: data.config.lrsApiUrl+'/api/lrs/login',
		  	method: 'POST',
		  	body: body,
		  	json: true
		}, 
		function(error, response, body) {
			// logger.debug("MsgId: ",msgId,"; FCC Transaction Cron: Login API ",  "response received: " + new Date(), "response: ", (body||error));
  			if (!error && response.statusCode == 200)
  			{
  				logger.debug("MsgId: ",msgId,"; EOD FCC Transaction Cron: Login API ",  "response received: " + new Date());
				//console.log("received auth token");
				if(body && body.authorizationToken) {
					data.headers.Authorization = 'JWT ' + body.authorizationToken;
			    
					resolve(data);
				} else {
					// data.res.status(400);
					var errorMsg = { "error": { "code": "1014", "messages": "Unable to generate Authorization token" } };
					//console.error("MsgId: ", data.timestamp, "Error: FCC Transaction Cron ", ": date: "+ new Date(), errorMsg);
					logger.error("MsgId: ", data.timestamp, "Error: EOD FCC Transaction Cron ", ": date: "+ new Date(), errorMsg);
					data.fccSyncErrorMessage = "Error while login to ODP";
                    data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
                    data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                    sendEmail(data)
                        .catch((error) => {
                            console.log(error);
                        });
					reject(errorMsg);
					//res.json(errorMsg);
				}
			    
			}
			else 
			{
				logger.debug("MsgId: ",msgId,"; EOD FCC Transaction Cron: Login API ",  "response received: " + new Date(), "response: ", (body||error));
			    var errorMsg = {"error": {"code" : "1005", "messages": "Error in connecting to login API"}};
		     	if(body && body.error) {
		      		errorMsg = body;
		      	}
		      	//console.error("MsgId: ",msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
        		logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
		     	data.fccSyncErrorMessage = "Error while login to ODP";
                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                sendEmail(data)
                    .catch((error) => {
                        console.log(error);
                    });
		     	reject(errorMsg);
			}
		});
	});
};

function getSchedule(data) {
	return new Promise((resolve, reject) => {
				
		//console.log(data.config.sandboxUrl + data.config.cronScheduleUrl);
		logger.info("MsgId: ",msgId,"; EOD FCC Transaction Cron: Get Cron Schedule ",  "request received: " + new Date());
		requestRetry(
		{
		  	headers: data.headers,      
		  	url: data.config.sandboxUrl + data.config.cronScheduleUrl,
		  	method: 'GET',
		  	json: true,
		  	maxAttempts: data.maxAttempts,   // (default) try 5 times
            retryDelay: data.retryDelay,  // (default) wait for 5s before trying again
            retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
		}, 
		function(error, response, body) {
			logger.info("MsgId: ",msgId,"; EOD FCC Transaction Cron: Get Cron Schedule ",  "response received: " + new Date());
			logger.debug("MsgId: ",msgId,"; EOD FCC Transaction Cron: Get Cron Schedule ",  "response received: " + new Date(), "response: ", (body||error));
  			if (!error && response.statusCode == 200) 
  			{
				if(body && body[0]) {
					if(body[0].cronSchedule && body[0].cronSchedule.eodFccTransaction && body[0].cronSchedule.eodFccTransaction.time) {
						// data.cronScheduleRunning = body[0].cronSchedule.fccTransaction.running;
						data.cronScheduleId = body[0]._id;
						data.minutes = body[0].cronSchedule.eodFccTransaction.time;
						var time = data.minutes.split(':');
						//console.log("time: ", time);
						data.schedule = ''+time[2]+' '+time[1]+' '+time[0]+' * * *';
						//data.schedule = '0 18 13 * * *'; 
						//if(data.minutes >= 60) {
						//	data.hours = (data.minutes)/60;
						//	data.schedule = '0 0 */'+data.hours+' * * *';
						//} else {
						//	data.schedule = '0 */'+data.minutes+' * * * *';
						//}
						//console.debug("MsgId: ", msgId, "; FCC Transaction Cron: ",  "request received: " + new Date(), "request: ", data.schedule);
						logger.debug("MsgId: ",msgId,"; EOD FCC Transaction Cron: ",  "request received: " + new Date(), "request: ", data.schedule);

						if(body[0].cronSchedule.eodFccTransaction.lastRecordProcessedTime) {
							data.cronScheduleRunning = body[0].cronSchedule.eodFccTransaction.running;
							data.cronScheduleToStart = body[0].cronSchedule.eodFccTransaction.toStart;
							// data.schedule = "0 54 11 * * *";
							// data.schedule = "*/10 * * * * *";
							// scheduler(data);
							resolve(data);
						} else {
							var errorMsg = {"error": {"code" : "1012", "messages": "Last Record Processed Time not defined For EOD FCC/FCR sync"}};
							//console.error("MsgId: ",msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
							logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
							data.fccSyncErrorMessage = "Kindly provide Last Record Processed Time for EOD FCC Transaction in Cron Schedule";
			                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
			                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
			                sendEmail(data)
			                    .catch((error) => {
			                        console.log(error);
			                    });
							reject(errorMsg);
						}
					} else {
						var errorMsg = {"error": {"code" : "1032", "messages": "Time is not defined For EOD FCC/FCR sync"}};
						//console.error("MsgId: ",msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
						logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
						data.fccSyncErrorMessage = "Kindly provide time for EOD FCC Transaction in Cron Schedule";
		                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
		                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
		                sendEmail(data)
		                    .catch((error) => {
		                        console.log(error);
		                    });
						reject(errorMsg);
					}
				} else {
					var errorMsg = {"error": {"code" : "1032", "messages": "Time is not defined For EOD FCC/FCR sync"}};
					//console.error("MsgId: ",msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
					logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
					data.fccSyncErrorMessage = "Kindly provide time for EOD FCC Transaction in Cron Schedule";
	                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
	                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
	                sendEmail(data)
	                    .catch((error) => {
	                        console.log(error);
	                    });
					reject(errorMsg);
				}
			    
			}
			else 
			{
			    var errorMsg = {"error": {"code" : "1004", "messages": (body?(body.messages?((body.messages && !body.code)?body.messages:body):body):error)}};
			    //console.error("MsgId: ",msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
        		logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
		     	data.fccSyncErrorMessage = "Error while getting Scheduler frequency for EOD FCC Transaction from Cron Schedule";
                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                sendEmail(data)
                    .catch((error) => {
                        console.log(error);
                    });
		     	reject(errorMsg);
		     	//console.log(errorMsg);
  			}
		});
	});
};

function updateFlag(data) {
    return new Promise((resolve, reject) => {
    	if(data.cronScheduleRunning) {
    	var body = {
            "cronSchedule": {
                "eodFccTransaction": {
                    "running": false
                }
            }
        };
        requestRetry(
        {
            headers: data.headers,      
            url: data.config.sandboxUrl + data.config.cronScheduleUrl + '/' + data.cronScheduleId,
            method: 'PUT',
            body: body,
            json: true,
            maxAttempts: data.maxAttempts,   // (default) try 5 times
            retryDelay: data.retryDelay,  // (default) wait for 5s before trying again
            retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
        }, 
        function(error, response, body) {
            if (!error && response.statusCode == 200) 
            {
            	// scheduler(data);
             //    resolve(data);
             	if(body.cronSchedule.eodFccTransaction.toStart) {
             		scheduler(data);
                	resolve(data);
             	} else {
             		var errorMsg = {"error": {"code" : "1104", "messages": "EOD FCC Scheduler has been stopped"}};
	                logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
	                data.fccSyncErrorMessage = "Kindly turn on 'To Start' of EOD FCC Transaction in Cron Schedule to start the scheduler";
	                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
	                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
	                sendEmail(data)
	                    .catch((error) => {
	                        console.log(error);
	                    });
	                reject(errorMsg);
             	}
            }
            else 
            {
                var errorMsg = {"error": {"code" : "1004", "messages": (body?(body.messages?((body.messages && !body.code)?body.messages:body):body):error)}};
                logger.error("MsgId: ", msgId,": Error: EOD FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
                data.fccSyncErrorMessage = "Error while checking if previous process is running in EOD FCC Transaction";
                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                sendEmail(data)
                    .catch((error) => {
                        console.log(error);
                    });
                reject(errorMsg);
            }
        });
    	} else {
    		if(data.cronScheduleToStart) {
    			scheduler(data);
                resolve(data);
    		} else {
    		    var errorMsg = { "error": { "code": "1104", "messages": "EOD FCC Scheduler has been stopped" } };
    		    logger.error("MsgId: ", msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
    		    data.fccSyncErrorMessage = "Kindly turn on 'To Start' of EOD FCC Transaction in Cron Schedule to start the scheduler";
    		    data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
    		    data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
    		    sendEmail(data)
    		        .catch((error) => {
    		            console.log(error);
    		        });
    		    reject(errorMsg);
    		}
    	}
    });
};

function scheduler(data) {
	cron.schedule(data.schedule, function(){
		//console.log("scheduledAt: ", data.schedule);
		/*data.waitCount = 0;
		getToken(data)
			.then(getTimestamp)
			.catch((error) => {
				console.log(error);
			});*/
		 oracleDb.getData(msgId);
  		//console.log("dateeee: "+ new Date());
	});
};

/*function getTimestamp(data) {
    return new Promise((resolve, reject) => {
        requestRetry(
        {
            headers: data.headers,      
            url: data.config.sandboxUrl + data.config.cronScheduleUrl,
            method: 'GET',
            json: true,
            maxAttempts: data.maxAttempts,   // (default) try 5 times
            retryDelay: data.retryDelay,  // (default) wait for 5s before trying again
            retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
        }, 
        function(error, response, body) {
            if (!error && response.statusCode == 200) 
            {
                data.cronScheduleId = body[0]._id;
                console.log("running: ", body[0].cronSchedule.fccTransaction.running)
                if(body[0].cronSchedule.fccTransaction.running) {
                	var count = 0;
                	if((((body[0].cronSchedule.fccTransaction.frequencyInMin)/10) > 0) && (((body[0].cronSchedule.fccTransaction.frequencyInMin)%10) != 0)) {
                		count = (body[0].cronSchedule.fccTransaction.frequencyInMin)/10;
                	} else {
                		count = ((body[0].cronSchedule.fccTransaction.frequencyInMin)/10)-1;
                	}
                    if(data.waitCount >= count) { //(((body[0].cronSchedule.fccTransaction.frequencyInMin)/10)-1)
                        var errorMsg = {"error": {"messages": "Number of retries exceeded"}};
                        reject(errorMsg);
                    } else {
                        // var toTime = (data.toTime && data.retry)?data.toTime:moment(new Date()).format("DD-MMM-YYYY HH:mm:ss");
                        data.waitCount++;
                        setTimeout(function() { 
                        	getToken(data)
                            	.then(getTimestamp)
                            	.catch((error) => {
                            		console.log(error)
                            	})
                        }, 10*60000);
                        var errorMsg = {"error": {"messages": "Previous process is still running"}};
                        logger.error("MsgId: ", msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
                        reject(errorMsg);
                    }
                    
                } else {
                    data.timestamp = body[0].cronSchedule.fccTransaction.lastRecordProcessedTime;
                    data.minutes = body[0].cronSchedule.fccTransaction.frequencyInMin;
                    oracleDb.getData(msgId);
                    resolve(data);
                }
                
            }
            else 
            {
                var errorMsg =8 {"error": {"code" : "1004", "messages": (body?(body.messages?((body.messages && !body.code)?body.messages:body):body):error)}};
                logger.error("MsgId: ", msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
                // data.fccSyncErrorMessage = "Error while checking if previous process is running: "+ JSON.stringify(errorMsg.error.messages);
                data.fccSyncErrorMessage = "Error while checking if previous process is running";
                data.fccSyncError = errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages;
                data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                sendEmail(data)
                    .catch((error) => {
                        console.log(error);
                    });
                reject(errorMsg);
            }
        });
    });
};*/

function sendEmail(data) {
    return new Promise((resolve, reject) => {
        var body = {
            templateID: data.fccSyncTemplate,
            params: [{"FromDate" : moment(new Date()).format("DD-MMM-YYYY HH:mm:ss")}, {"Message" : data.fccSyncErrorMessage}, {"Error" : data.fccSyncError}],
            channelName: "LRS"
        }; 
        logger.info("MsgId: ", msgId, "; EOD FCC Transaction Cron: Send Email ", "request received: " + new Date());
        logger.debug("MsgId: ", msgId, "; EOD FCC Transaction Cron: Send Email ", "request received: " + new Date(), "request: ", body);
        request({
            url: data.config.sendEmailUrl,
            method: 'POST',
            body: body,
            json: true
        }, function(error, response, body) {
            logger.info("MsgId: ", msgId, "; EOD FCC Transaction Cron: Send Email ", "response received: " + new Date());
            logger.debug("MsgId: ", msgId, "; EOD FCC Transaction Cron: Send Email ", "response received: " + new Date(), "response: ", (body||error));
            if (!error && response.statusCode == 200) {
                logger.debug("MsgId: ", msgId, "; EOD FCC Transaction Cron ", "request processed: " + new Date(), "response: Email sent Successfully");
                resolve(data);

            } else {
                var errorMsg = { "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                logger.error("MsgId: ", msgId, "Error: EOD FCC Transaction Cron ", ": date: " + new Date(), errorMsg);
                reject(errorMsg);
                //console.log("Error occurred while sending Transaction Report");
            }
        });
    });
}