'use strict';

var configDetails = require('../config/configUrl.js');
const log4js = require('log4js');

const logger = log4js.getLogger(''); // to log errors in a log file
var timestamp = new Date().getTime();

const clusterID = process.env.NATS_CLUSTERID || "test-cluster"
const clientID = `PRODUCER-${Math.ceil(Math.random() * 100000)}`
//var server = "nats://"+process.env.NATS_IP?process.env.NATS_IP:"localhost"+":"+process.env.NATS_PORT?process.env.NATS_PORT:"4222";
var server = "nats://"+configDetails.config.natsIp+":"+configDetails.config.natsPort;
// const channelName = "syslogs"
// console.log("server: ", server)
var stan = require('node-nats-streaming').connect(clusterID, clientID, server);

stan.on("connect", () => {
    console.log(`${clientID} connected to cluster ${clusterID}`);
    //publish()
})

stan.on('error', _ => console.log(_));

module.exports = {
  publish
}

function publish(message, channelName){
  //console.log("publish()::::::::::::: ", channelName, " message: ", message)
  logger.info("MsgId: ",timestamp, "; Nats Streaming: ",  "request received: " + new Date(), " channelName: ", channelName, " message: ", message);
  stan.publish(channelName, message, (_err, _guid) => {
    if(_err) {
    	logger.error("MsgId: ",timestamp, "; Error: Nats Streaming: ",  "request received: " + new Date(), " error: ", _err, " channelName: ", channelName);
    	// console.log("ereror nats: ", _err)
    } else logger.info("MsgId: ",timestamp, "; Nats Streaming: ",  "request received: " + new Date(), " guid: ", _guid, " message: ", message);
  });
}