'use strict';

const fs = require('fs'),
	path = require('path'),
	xmlReader = require('read-xml');
const parser = require('xml2js').Parser();
const log4js = require('log4js');
const logger = log4js.getLogger('logs');

var configurationData = '';

var FILE = path.join(__dirname, '../config/generic.xml');

var msgId = new Date().getTime();
try {
xmlReader.readXML(fs.readFileSync(FILE), function(err, data) {
	if (err) {
		console.log("messages: Error in reading XML Configuration file");
	} else {
		parser.parseString(data.content, function (err, result) {
			if(err) {
				logger.error("MsgId: ", msgId, "; XML processing error: "+ err + " , " + new Date());
				console.log("messages: Error in parsing XML configuration content");
			} else {
				configurationData = result.config;
				logger.info("MsgId: ", msgId, "; XML processed: " + new Date());
	        	console.log("messages: Converted XML configuration data to JSON");
			}
	        
	    });
	}
	
});
} catch(e) {
	logger.error("MsgId: ", msgId, "; XML processing error: "+ e + " , " + new Date());
}

module.exports = {
	configurationData
};