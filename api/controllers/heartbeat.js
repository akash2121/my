'use strict';

//const request = require('request');

const log4js = require('log4js');

const logger = log4js.getLogger(''); // to log errors in a log file


module.exports = {
    heartbeat
}
var timestamp = new Date().getTime();

function heartbeat(req, res) {
    
    logger.info("MsgId: ",timestamp, "; Liveness Probe: ",  "I am alive!!! " + new Date());

    res.json({"message": "I am alive!!!"});

};