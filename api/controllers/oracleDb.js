'use strict';

const oracledb = require('oracledb');
const request = require('request');
var requestRetry = require('requestretry');
// const k8sDriven = require('../config/k8sDrivenConfig.js');
var aes256 = require('aes256');
const moment = require('moment');
const async = require('async');
const log4js = require('log4js');
const logger = log4js.getLogger('logs'); // to log errors in a log file
const mask = require('json-masker');
const util = require('util');
const json2xls = require('json2xls');
var fs = require('fs');
var FormData = require('form-data');
var form = new FormData();
var data;
// var waitCount = 0;

const stanProducer = require('./stanProducer.js');

module.exports = {
    getData
};

function getData(msgId, fromTime, toTime, retry, waitCount) { // function to connect Oracle Database
    var k8sDriven = require('../config/k8sDrivenConfig.js');
    var configFile = require('./readXML.js');

    var xmlConfig = configFile.configurationData;
    var configDetails = require('../config/configUrl.js');
    if (xmlConfig) {
        data = {
            headers: {
                'Content-Type': 'application/json',
            },
            mappings: xmlConfig.mappings[0],
            oracle_config: xmlConfig.oracle_fcc_config[0],
            config: configDetails.config,
            msgId: msgId,
            failedTransactions: [],
            fromTime: fromTime,
            toTime: toTime,
            retry: retry,
            k8sDriven: k8sDriven,
            successfulCount: 0,
            failedCount: 0,
            maxAttempts: process.env.RETRY_ATTEMPTS ? parseInt(process.env.RETRY_ATTEMPTS) : 5,
            retryDelay: process.env.RETRY_DELAY ? parseInt(process.env.RETRY_DELAY) : 1000
        };
        data.waitCount = retry ? waitCount : 0;
        var key = 'encryptedkey1223FoRlrsaPis';
        var password = data.config.esbAuth;
        data.esbAuth = aes256.decrypt(key, password);
        getToken(data)
            .then(getTimestamp)
            .then(updateToRunning)
            .then(oracleConnection)
            .then(getOracleData)
            .then(closeOracleConnection)
            .catch((error) => {
                console.log(error);
            });
    } else {
        logger.error("MsgId: ", msgId, "; EOD FCC Transaction Cron: ", "request processed : Error while processing XML file " + new Date());
    }
};

function getToken(data) {
    return new Promise((resolve, reject) => {
        var body = {
            "username": data.k8sDriven.omniDataPlatformUser,
            "password": data.k8sDriven.omniDataPlatformPassword,
            "encrypted": true
        };
        request({
                headers: data.headers,
                url: data.config.lrsApiUrl + '/api/lrs/login',
                method: 'POST',
                body: body,
                json: true
            },
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    if (body && body.authorizationToken) {
                        data.headers.Authorization = 'JWT ' + body.authorizationToken;
                        //data.authorization = 'JWT ' + body.authorizationToken;
                        if (!data.retry) {
                            data.resMsg = "EOD FCC Scheduler started at: " + new Date();
                            data.logType = data.config.logLevel;
                            data.message = "Request Received";
                            systemLogs(data)
                                .catch((error) => {
                                    logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), error);
                                });
                        }
                        resolve(data);
                    } else {
                        // res.status(400);
                        var errorMsg = { "error": { "code": "1014", "messages": "Unable to generate Authorization token" } };
                        logger.error("MsgId: ", data.msgId, "Error: EOD FCC Transaction Cron ", ": date: " + new Date(), errorMsg);
                        data.fccSyncErrorMessage = "Error while login to ODP";
                        data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                        data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                        sendEmail(data)
                            .catch((error) => {
                                console.log(error);
                            });
                        // res.json(errorMsg);
                    }
                } else {
                    var errorMsg = { "error": { "code": "1005", "messages": "Error in connecting to login API" } };
                    if (body && body.error) {
                        errorMsg = body;
                    }
                    logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                    data.fccSyncErrorMessage = "Error while login to ODP";
                    data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                    data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                    sendEmail(data)
                        .catch((error) => {
                            console.log(error);
                        });
                    reject(errorMsg);
                }
            });
    });
};

function getTimestamp(data) {
    return new Promise((resolve, reject) => {
        if (data.fromTime) {
            resolve(data);
        } else {
            requestRetry({
                    headers: data.headers,
                    url: data.config.sandboxUrl + data.config.cronScheduleUrl,
                    method: 'GET',
                    json: true,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        data.cronScheduleId = body[0]._id;
                        console.log("running: ", body[0].cronSchedule.eodFccTransaction.running, " count: ", data.waitCount, " totime ", data.toTime)
                        data.timestamp = body[0].cronSchedule.eodFccTransaction.lastRecordProcessedTime;
                        data.toTime = body[0].cronSchedule.eodFccTransaction.toTime;
                        data.minutes = body[0].cronSchedule.eodFccTransaction.time;
                        resolve(data);
                    } else {
                        var errorMsg = { "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                        logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                        data.resMsg = JSON.stringify(errorMsg);
                        data.logType = "ERROR";
                        data.message = "Response Received";
                        systemLogs(data)
                            .catch((error) => {
                                logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), error);
                            });
                        data.fccSyncErrorMessage = "Error from ODP while getting Last Record Procossed Time from Cron Schedule for EOD FCC Transaction";
                        data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                        data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                        sendEmail(data)
                            .catch((error) => {
                                console.log(error);
                            });
                        reject(errorMsg);
                    }
                });
        }
    });
};

function updateToRunning(data) {
    return new Promise((resolve, reject) => {
        if (data.fromTime) {
            resolve(data);
        } else {
            var body = {
                "cronSchedule": {
                    "eodFccTransaction": {
                        "running": true
                    }
                }
            };
            //console.log("body: ", body)
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Running State ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Running State ", "request received: " + new Date(), "request: ", body);
            requestRetry({
                    headers: data.headers,
                    url: data.config.sandboxUrl + data.config.cronScheduleUrl + '/' + data.cronScheduleId,
                    method: 'PUT',
                    body: body,
                    json: true,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Running State ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Running State ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && response.statusCode == 200) {
                        logger.debug("MsgId: ", data.msgId, "date:" + new Date() + "messages: Running state for EOD FCC Transaction updated");
                        resolve(data);
                    } else {
                        var errorMsg = { "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                        logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                        data.fccSyncErrorMessage = "Error from ODP while updating in Cron Schedule that process is running";
                        data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                        data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                        sendEmail(data)
                            .catch((error) => {
                                console.log(error);
                            });
                        reject(errorMsg);
                    }
                });
        }
    });
};

function oracleConnection(data) { // function to connect to Oracle Database
    return new Promise((resolve, reject) => {

        var key = 'encryptedkey1223FoRlrsaPis';
        var password = data.k8sDriven.databasePassword;
        password = aes256.decrypt(key, password);

        var regex = new RegExp('DBHOST', 'g');
        var connectionString = data.oracle_config.connect_string[0].replace(regex, data.k8sDriven.DBHOST);
        var regex = new RegExp('DBPORT', 'g');
        connectionString = connectionString.replace(regex, data.k8sDriven.DBPORT);
        var regex = new RegExp('DBSERVICE_NAME', 'g');
        connectionString = connectionString.replace(regex, data.k8sDriven.DBSERVICE_NAME);
        console.log("oracleString: ", connectionString);

        oracledb.getConnection({
                user: data.k8sDriven.databaseUser,
                password: password,
                connectString: connectionString
            },
            function(err, connection) {
                if (err) {
                    data.error = {
                        "code": "1016",
                        "messages": "Error while connecting to FCR/FCC DB: ",
                        err
                    };
                    data.fccSyncErrorMessage = "Error while connecting to FCR/FCC DB";
                    data.fccSyncError = "" + err;
                    data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                    sendEmail(data)
                        .catch((error) => {
                            console.log(error);
                        });
                    logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), data.error);
                    updateToNotRunning(data)
                        .catch((error) => {
                            console.log(error)
                        });
                    data.resMsg = JSON.stringify(data.error);
                    data.logType = "ERROR";
                    data.message = "Response Received";
                    systemLogs(data)
                        .catch((error) => {
                            logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), error);
                        });
                    reject(data.error);

                } else {
                    data.connection = connection;
                    resolve(data);
                }
            });
    });
};

function getOracleData(data) { // function to get data from Oracle Database
    return new Promise((resolve, reject) => {
        console.log("To time : ", data.toTime);
        if (data.toTime === "00:00:00") {
            data.startTime = data.timestamp ? data.timestamp : moment(new Date(), "DD-MMM-YYYY HH:mm:ss");
            data.endTime = moment(data.timestamp).add(1, 'days').format("DD-MMM-YYYY") + " " + data.toTime;
            data.processedTime = data.endTime;
            //console.log("date: ", data.startTime, data.endTime);
        } else {
            data.startTime = data.timestamp ? data.timestamp : moment(new Date(), "DD-MMM-YYYY HH:mm:ss");
            data.endTime = moment(data.timestamp).format("DD-MMM-YYYY") + " " + data.toTime;
            data.processedTime = moment(data.timestamp).add(1, 'days').format("DD-MMM-YYYY HH:mm:ss");
            //console.log("date: ", data.startTime, data.endTime, data.processedTime);
        }
        /*var time = moment(data.timestamp, "DD-MMM-YYYY HH:mm:ss");
        data.startTime = data.timestamp ? data.timestamp : moment(new Date(), "DD-MMM-YYYY HH:mm:ss");
        data.endTime = moment(data.startTime).add(1, 'days').format("DD-MMM-YYYY 00:00:00");
        console.log("date: ", data.startTime, data.endTime);*/
        data.schedulerId = 'EODFCC_' + data.startTime + '-' + data.endTime + '-' + moment(new Date(), "HH:mm:ss");
        data.startDate = moment(data.startTime).format("DD-MMM-YYYY");
        data.endDate = moment(data.endTime).format("DD-MMM-YYYY");
        //console.log("start Date: ", data.startDate, "  : ", data.startTime,  " eds: ", data.endTime, "schedulerID: ", data.schedulerId, "processedTime: ", data.processedTime);
        //var query = data.oracle_config.query1[0] + ' BETWEEN to_date(\''+data.startTime+'\', \'dd-MON-yyyy HH24:MI:SS\') AND to_date(\''+data.endTime+'\', \'dd-MON-yyyy HH24:MI:SS\') ' +data.oracle_config.query2[0] + ' BETWEEN to_date(\''+data.startTime+'\', \'dd-MON-yyyy HH24:MI:SS\') AND to_date(\''+data.endTime+'\', \'dd-MON-yyyy HH24:MI:SS\') ' +data.oracle_config.query3[0] + ' BETWEEN to_date(\''+data.startTime+'\', \'dd-MON-yyyy HH24:MI:SS\') AND to_date(\''+data.endTime+'\', \'dd-MON-yyyy HH24:MI:SS\') ' +data.oracle_config.query4[0] + ' BETWEEN to_date(\''+data.startTime+'\', \'dd-MON-yyyy HH24:MI:SS\') AND to_date(\''+data.endTime+'\', \'dd-MON-yyyy HH24:MI:SS\') ' +data.oracle_config.query5[0];

        var regex = new RegExp('STARTTIMESTAMP', 'g');
        var query = data.oracle_config.query[0].replace(regex, '\'' + data.startTime + '\'');
        regex = new RegExp('ENDTIMESTAMP', 'g');
        query = query.replace(regex, '\'' + data.endTime + '\'');
        regex = new RegExp('STARTDATE', 'g');
        query = query.replace(regex, '\'' + data.startDate + '\'');
        regex = new RegExp('ENDDATE', 'g');
        query = query.replace(regex, '\'' + data.endDate + '\'');
        // setTimeout(function() {

        // var query = data.oracle_config.query[0] + ' WHERE TIMESTAMP BETWEEN to_date(\''+data.startTime+'\', \'dd-MON-yyyy HH24:MI:SS\') AND to_date(\''+data.endTime+'\', \'dd-MON-yyyy HH24:MI:SS\') ';
        console.log("query: ", query);
        data.connection.execute(
            query,
            [], {
                outFormat: oracledb.OBJECT
            },
            function(err, result) {
                if (err) {
                    data.error = {
                        "code": "1020",
                        "messages": "Unable to fetch data from FCR/FCC: ",
                        err
                    };
                    logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), data.error);
                    data.fccSyncErrorMessage = "Unable to fetch data from FCR/FCC";
                    data.fccSyncError = "" + err;
                    data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                    sendEmail(data)
                        .catch((error) => {
                            console.log(error);
                        });
                    updateToNotRunning(data)
                        .catch((error) => {
                            console.log(error)
                        });
                    data.resMsg = JSON.stringify(data.error);
                    data.logType = "ERROR";
                    data.message = "Response Received";
                    systemLogs(data)
                        .catch((error) => {
                            logger.error("MsgId: ", data.msgId, ": Error: EOD Fetch Customer ID Cron ", "date:" + new Date(), error);
                        });
                    reject(data.error);
                } else {
                    // console.log("Number of records received from Oracle Database: ", result.rows.length);
                    // console.log("body: ", result.rows);
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: DB Query Data ", "Number of records received from Oracle Database: ", result.rows.length, " ", new Date());
                    let queryList = result.rows.map(array => ({
                        CONTRACT_REF_NO: array.CONTRACT_REF_NO,
                        TXN_DATE: array.TXN_DATE,
                        LCY_AMOUNT: array.LCY_AMOUNT,
                        USD_EQUV_AMT: array.USD_EQUV_AMT,
                        BENEFICIARY: array.BENEFICIARY,
                        BILL_CCY: array.BILL_CCY,
                        CR_AMOUNT: array.CR_AMOUNT,
                        EX_RATE: array.EX_RATE,
                        RBI_PURPOSE_CODE: array.RBI_PURPOSE_CODE,
                        NAM_CUST_FULL: array.NAM_CUST_FULL,
                        FLG_CUST_TYP: array.FLG_CUST_TYP,
                        TXT_CUST_TYP: array.TXT_CUST_TYP,
                        BOOK_DATE: array.BOOK_DATE,
                        FIELD_VALUE: array.FIELD_VALUE,
                        LRS_FLAG: array.LRS_FLAG,
                        TXN_LITERAL: array.TXN_LITERAL,
                        ACCOUNT_NO: array.ACCOUNT_NO,
                        UDF_VALUE: array.UDF_VALUE
                    }));
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: DB Query Data ", "Records from query: ", queryList, " ", new Date());
                    data.resMsg = "Count: " + result.rows.length + "\n Body: " + JSON.stringify(queryList);
                    data.logType = data.config.logLevel;
                    data.message = data.startTime + " to " + data.endTime;
                    systemLogs(data)
                        .catch((error) => {
                            logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), error);
                        });
                    // updateTimestamp(data)
                    //     .catch((error) => {
                    //         console.log(error);
                    //     });
                    if (result.rows.length === 0) {
                        data.successfulCount = 0, data.failedCount = 0;
                        data.failedTransactions = [];
                        // updateTimestamp(data)
                        // .catch((error) => {
                        //     console.log(error);
                        // });
                        updateFccTransactions(data)
                            .then(updateTimestamp)
                            .catch((error) => {
                                console.log(error);
                            });
                        data.resMsg = "0 transactions received from FCC";
                        data.logType = data.config.logLevel;
                        data.message = "Response Received";
                        systemLogs(data)
                            .catch((error) => {
                                logger.error("MsgId: ", data.msgId, ": Error: EOD Fetch Customer ID Cron ", "date:" + new Date(), error);
                            });
                        var message = { "messages": "No transactions received from database" };
                        reject(message);
                    } else {


                        var oracleData = JSON.stringify(result.rows);
                        var odpData;

                        for (var i = 0; i < data.mappings.mapping.length; i++) {
                            oracleData = oracleData.split(data.mappings.mapping[i].from_column[0]).join(data.mappings.mapping[i].to_column[0]);
                        }

                        oracleData = JSON.parse(oracleData);
                        data.oracleData = oracleData;

                        data.successfulCount = 0, data.failedCount = 0;

                        data.failedTransactions = [];
                        data.completedTransactions = [];
                        async.eachSeries(data.oracleData, function(oracleData, cb) {
                            transactionRealize(oracleData, cb)
                                .then(getTokenizedPan)
                                .then(getTokenizedAadhaarNo)
                                .then(getTokenizedCustId)
                                .then(getTokenizedCardNo)
                                .then(addFailedTransaction)
                                .then((data) => {
                                    cb();
                                })
                                .catch((error) => {
                                    console.log(error);
                                    cb();
                                });

                        }, function(err) {
                            //console.log("All transactions realized");
                            // updateTimestamp(data)
                            // .catch((error) => {
                            //     console.log(error);
                            // });
                            createFile(data)
                                .then(updateFccTransactions)
                                //.then(updateTimestamp)
                                .then(createFccSuccessFile)
                                .then((data) => {
                                    if (data.emailBody === undefined) {
                                        updateTimestamp(data)
                                            .catch((error) => {
                                                console.log(error);
                                            });
                                        data.resMsg = "Out of " + (data.oracleData.length) + " Transactions, " + data.successfulCount + " transactions were successful and " + data.failedCount + " transactions failed";
                                        data.logType = data.config.logLevel;
                                        data.message = "Response Received";
                                        systemLogs(data)
                                            .catch((error) => {
                                                logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), error);
                                            });
                                    } else {
                                        sendEmail(data)
                                            .then(updateTimestamp)
                                            .catch((error) => {
                                                console.log(error);
                                            });
                                        data.resMsg = "Out of " + (data.oracleData.length) + " Transactions, " + data.successfulCount + " transactions were successful and " + data.failedCount + " transactions failed";
                                        data.logType = data.config.logLevel;
                                        data.message = "Response Received";
                                        systemLogs(data)
                                            .catch((error) => {
                                                logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), error);
                                            });
                                    }
                                })
                                .catch((error) => {
                                    updateToNotRunning(data)
                                        .catch((error) => {
                                            console.log(error)
                                        });
                                    console.log(error);
                                });
                            /*data.resMsg = "Out of " + (data.oracleData.length) + " Transactions, " + data.successfulCount + " transactions were successful and " + data.failedCount + " transactions failed";
                            data.logType = data.config.logLevel;
                            data.message = "Response Received";
                            systemLogs(data)
                                .catch((error) => {
                                    logger.error("MsgId: ", data.msgId, ": Error: FCC Transaction Cron ", "date:" + new Date(), error);
                                });*/
                        });

                        resolve(data);
                    }
                }
            }
        );
        // }, 5*60000);
    });
};

function closeOracleConnection(data) { // function to close connection to Oracle Database
    return new Promise((resolve, reject) => {
        data.connection.release(function(err) {
            if (err) {
                data.error = {
                    "code": "1023",
                    "messages": "Error while closing connection to FCR/FCC DB: ",
                    err
                };
                logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), data.error);
                reject(data.error);
            } else {
                console.log("messages: Connection to Oracle Database closed");
            }
        });
    });
};

function transactionRealize(oracleData) { // function to realize the transactions
    return new Promise((resolve, reject) => {

        var body = {
            "pan": oracleData.pan,
            "fromCustomerType": oracleData.fromCustomerType,
            "aadhaarNo": oracleData.aadhaarNo,
            "cardNo": oracleData.cardNo,
            "transactionAmount": oracleData.transactionAmount,
            "bookedDate": oracleData.bookedDate,
            "beneficiary": oracleData.beneficiary,
            "customerName": oracleData.customerName,
            "sourceRefNo": oracleData.sourceRefNo,
            "currency": oracleData.currency,
            "code": oracleData.code,
            "fccRefNo": oracleData.fccRefNo,
            "exchangeRate": oracleData.exchangeRate,
            "flgCustType": oracleData.flgCustType,
            "transactionDateTime": oracleData.transactionDateTime,
            "inrAmount": oracleData.inrAmount,
            "channelName": "FCC",
            "txnLiteral": oracleData.txnLiteral,
            //"custId": oracleData.custId,
            "custId": null,
            "usdRate": oracleData.usdRate,
            "usdAmount": oracleData.usdAmount,
            "lovDesc": oracleData.purposeDescription,
            "lrsFlag": oracleData.status,
            "accountNo": oracleData.accountNo,
            "authorized": true,
            "schedulerId": data.schedulerId
        };
        logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Transaction Realization ", "request received: " + new Date());
        logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Transaction Realization ", "request received: " + new Date(), "request: ", body);
        /*data.transactionToProcess = body;*/
        oracleData.channelName = "FCC";
        oracleData.authorized = true;
        data.transactionToProcess = body;
        data.header = {
            "Content-Type": "application/json",
            "token": data.headers.Authorization
        };
        request({
            headers: data.header,
            url: data.config.lrsApiUrl + "/api/lrs/transactionRealization",
            method: 'POST',
            body: body,
            json: true
        }, function(error, response, body) {
            //console.log("sdadadddddddddddddddd: ", body, "  SDADSAD: ", response, "dfsdf: ", data.failedTransactions)
            if (!error && response.statusCode == 200) {
                data.failed = false;
                data.completedTransactions.push(data.transactionToProcess);
                //console.log("completedTransactions: ", data.completedTransactions, data.completedTransactions.length);
                data.successfulCount++;
                resolve(data);
            } else {
                var errorMsg = { "error": { "code": "1025", "messages": "Error while connecting to Transaction Realization API" } };
                //console.log(errorMsg);
                // if (body && body.error) {
                //     errorMsg = body;
                // }
                errorMsg = body ? body : (error ? error : errorMsg);
                data.failedCount++;
                //data.transactionToProcess?(data.transactionToProcess.error = (errorMsg.error.messages.message?errorMsg.error.messages.message:errorMsg.error.messages)):data.transactionToProcess;
                if (data.transactionToProcess) {
                    if (errorMsg.error) {
                        if (errorMsg.error.messages) {
                            if (errorMsg.error.messages.message) {
                                if (typeof errorMsg.error.messages.message === "string") {
                                    data.transactionToProcess.error = errorMsg.error.messages.message;
                                } else {
                                    data.transactionToProcess.error = JSON.stringify(errorMsg.error.messages.message);
                                }
                            } else {
                                if (typeof errorMsg.error.messages === "string") {
                                    data.transactionToProcess.error = errorMsg.error.messages;
                                } else {
                                    data.transactionToProcess.error = JSON.stringify(errorMsg.error.messages);
                                }
                            }
                        } else {
                            data.transactionToProcess.error = JSON.stringify(errorMsg.error);
                        }
                    } else {
                        data.transactionToProcess.error = JSON.stringify(errorMsg);
                    }
                    // data.transactionToProcess.error = JSON.stringify(errorMsg);
                } else {
                    data.transactionToProcess = {};
                }

                // var failedTransactions = data.failedTransactions;
                //             failedTransactions.push(data.transactionToProcess);
                //             data.failedTransactions = failedTransactions;
                data.failed = true;

                //console.error("MsgId: ",data.msgId,": Error: FCC Transaction Cron ", "date:" + new Date() ,errorMsg);
                logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                //reject(errorMsg);
                resolve(data);
            }
        });
    });
};

function getTokenizedPan(data) {
    return new Promise((resolve, reject) => {
        if (data.transactionToProcess.pan && data.failed) {
            var body = {
                "naeUser": data.config.panNaeUser,
                "user2": data.config.panUser2,
                "value": data.transactionToProcess.pan,
                "tableName": data.config.panTableName,
                "format": data.config.tokenizeFormat
            };
            var idVaultBody = {
                "naeUser": data.config.panNaeUser,
                "user2": data.config.panUser2,
                "value": aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.pan),
                "tableName": data.config.panTableName,
                "format": data.config.tokenizeFormat
            };
            // idVaultBody.value = aes256.encrypt("encrypted734329sensitive3ndsm832", idVaultBody.value);
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: PAN Tokenize ID Vault ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: PAN Tokenize ID Vault ", "request received: " + new Date(), "request: ", idVaultBody);
            data.header = {
                "Content-Type": "application/json",
                "Authorization": "Basic " + data.esbAuth
            }
            var startingTime = new Date()
            requestRetry({
                    headers: data.header,
                    url: data.config.tokenizeUrl,
                    method: 'POST',
                    body: body,
                    json: true,
                    timeout: data.config.idVaultApiTimeout,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: function(err, response, body, options) {
                        var regExp = new RegExp("^(2[0-9][0-9])$");
                        if (response && !regExp.test(response.statusCode)) {
                            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: PAN Tokenize ID Vault ", "Exception response received: " + new Date());
                            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: PAN Tokenize ID Vault ", "Exception response received: " + new Date(), "response: ", body || err);

                            data.resMsg = JSON.stringify(body || err);
                            data.logType = "ERROR";
                            data.message = "Response Received";
                            systemLogs(data)
                                .catch((error) => {
                                    logger.error("MsgId: ", data.msgId, ": Error: EOD Fetch Customer ID Cron ", "date:" + new Date(), error);
                                });
                        }
                        //var regExp = new RegExp("^(?!2[0-9][0-9])$");
                        return response && !regExp.test(response.statusCode);
                        //return (response && response.statusCode !== 200);
                    } //requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    var endingTime = new Date();
                    logger.info("MsgId: ", data.msgId, "; Response time from PAN Tokenize ID Vault API: ", (endingTime - startingTime) / 1000, " sec");
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: PAN Tokenize ID Vault ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: PAN Tokenize ID Vault ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
                        if (body && body.token) {
                            data.transactionToProcess.pan = body.token;
                            resolve(data);
                        } else {
                            data.transactionToProcess.pan = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.pan);
                            var errorMsg = { "status": "Fail", "error": { "code": "1072", "messages": "Invalid response from PAN Tokenize API. Saved the encrypted PAN" } };
                            //data.transactionToProcess.result = "Fail";
                            data.transactionToProcess.error = data.transactionToProcess.error ? (data.transactionToProcess.error + ", " + errorMsg.error.messages) : errorMsg.error.messages;
                            //data.transactionToProcess.realized = true;
                            resolve(data);
                        }
                    } else {
                        data.transactionToProcess.pan = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.pan);
                        var errorMsg = { "error": { "code": "1071", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };

                        // data.transactionToProcess.error = data.transactionToProcess.error?(data.transactionToProcess.error+", "+JSON.stringify(errorMsg)+ ". Saved the encrypted PAN"):(JSON.stringify(errorMsg)+ ". Saved the encrypted PAN");
                        if (typeof errorMsg.error.messages === "string") {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + errorMsg.error.messages + ", Saved the encrypted PAN" : errorMsg.error.messages + ", Saved the encrypted PAN";
                        } else {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted PAN" : JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted PAN";
                        }
                        //data.transactionToProcess.realized = true;
                        resolve(data);

                    }
                });
        } else {
            resolve(data);
        }
    });
};

function getTokenizedAadhaarNo(data) {
    return new Promise((resolve, reject) => {
        if (data.transactionToProcess.aadhaarNo && data.failed) {
            var body = {
                "naeUser": data.config.aadhaarNaeUser,
                "user2": data.config.aadhaarUser2,
                "value": data.transactionToProcess.aadhaarNo,
                "tableName": data.config.aadhaarNoTableName,
                "format": data.config.tokenizeFormat
            };
            var idVaultBody = {
                "naeUser": data.config.aadhaarNaeUser,
                "user2": data.config.aadhaarUser2,
                "value": aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.aadhaarNo),
                "tableName": data.config.aadhaarNoTableName,
                "format": data.config.tokenizeFormat
            };
            // idVaultBody.value = aes256.encrypt("encrypted734329sensitive3ndsm832", idVaultBody.value);
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Aadhaar No Tokenize ID Vault ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Aadhaar No Tokenize ID Vault ", "request received: " + new Date(), "request: ", idVaultBody);
            data.header = {
                "Content-Type": "application/json",
                "Authorization": "Basic " + data.esbAuth
            }
            var startingTime = new Date();
            requestRetry({
                    headers: data.header,
                    url: data.config.tokenizeUrl,
                    method: 'POST',
                    body: body,
                    json: true,
                    timeout: data.config.idVaultApiTimeout,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: function(err, response, body, options) {
                        var regExp = new RegExp("^(2[0-9][0-9])$");
                        if (response && !regExp.test(response.statusCode)) {
                            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Aadhaar No Tokenize ID Vault ", "Exception response received: " + new Date());
                            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Aadhaar No Tokenize ID Vault ", "Exception response received: " + new Date(), "response: ", body || err);

                            data.resMsg = JSON.stringify(body || err);
                            data.logType = "ERROR";
                            data.message = "Response Received";
                            systemLogs(data)
                                .catch((error) => {
                                    logger.error("MsgId: ", data.msgId, ": Error: EOD Fetch Customer ID Cron ", "date:" + new Date(), error);
                                });
                        }
                        //var regExp = new RegExp("^(?!2[0-9][0-9])$");
                        return response && !regExp.test(response.statusCode);
                        //return (response && response.statusCode !== 200);
                    } //requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    var endingTime = new Date();
                    logger.info("MsgId: ", data.msgId, "; Response time from Aadhaar No Tokenize ID Vault API: ", (endingTime - startingTime) / 1000, " sec");
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Aadhaar No Tokenize ID Vault ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Aadhaar No Tokenize ID Vault ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
                        if (body && body.token) {
                            data.transactionToProcess.aadhaarNo = body.token;
                            resolve(data);
                        } else {
                            data.transactionToProcess.aadhaarNo = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.aadhaarNo);
                            var errorMsg = { "status": "Fail", "error": { "code": "1073", "messages": "Invalid response from Aadhaar No Tokenize API. Saved the encrypted Aadhaar No" } };
                            //data.transactionToProcess.result = "Fail";
                            // data.transactionToProcess.error = data.transactionToProcess.error?(data.transactionToProcess.error+", "+JSON.stringify(errorMsg)):JSON.stringify(errorMsg);
                            data.transactionToProcess.error = data.transactionToProcess.error ? (data.transactionToProcess.error + ", " + errorMsg.error.messages) : errorMsg.error.messages;
                            //data.body.realized = true;
                            resolve(data);
                        }
                    } else {
                        data.transactionToProcess.aadhaarNo = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.aadhaarNo);
                        var errorMsg = { "error": { "code": "1071", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };

                        // data.transactionToProcess.error = data.transactionToProcess.error?(data.transactionToProcess.error+", "+JSON.stringify(errorMsg)+ ". Saved the encrypted Aadhaar No"):(JSON.stringify(errorMsg)+ ". Saved the encrypted Aadhaar No");
                        if (typeof errorMsg.error.messages === "string") {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + errorMsg.error.messages + ", Saved the encrypted Aadhaar No" : errorMsg.error.messages + ", Saved the encrypted Aadhaar No";
                        } else {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted Aadhaar No" : JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted Aadhaar No";
                        }
                        //data.transactionToProcess.realized = true;
                        resolve(data);

                    }
                });
        } else {
            resolve(data);
        }
    });
};

function getTokenizedCustId(data) {
    return new Promise((resolve, reject) => {
        if (data.transactionToProcess.custId && data.failed) {
            var body = {
                "naeUser": data.config.custidNaeUser,
                "user2": data.config.custidUser2,
                "value": data.transactionToProcess.custId,
                "tableName": data.config.custIdTableName,
                "format": data.config.tokenizeFormat
            };
            var idVaultBody = {
                "naeUser": data.config.custidNaeUser,
                "user2": data.config.custidUser2,
                "value": aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.custId),
                "tableName": data.config.custIdTableName,
                "format": data.config.tokenizeFormat
            };
            // idVaultBody.value = aes256.encrypt("encrypted734329sensitive3ndsm832", idVaultBody.value);
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Customer ID Tokenize ID Vault ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Customer ID Tokenize ID Vault ", "request received: " + new Date(), "request: ", idVaultBody);
            data.header = {
                "Content-Type": "application/json",
                "Authorization": "Basic " + data.esbAuth
            }
            var startingTime = new Date();
            requestRetry({
                    headers: data.header,
                    url: data.config.tokenizeUrl,
                    method: 'POST',
                    body: body,
                    json: true,
                    timeout: data.config.idVaultApiTimeout,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: function(err, response, body, options) {
                        var regExp = new RegExp("^(2[0-9][0-9])$");
                        if (response && !regExp.test(response.statusCode)) {
                            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Customer ID Tokenize ID Vault ", "Exception response received: " + new Date());
                            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Customer ID Tokenize ID Vault ", "Exception response received: " + new Date(), "response: ", body || err);

                            data.resMsg = JSON.stringify(body || err);
                            data.logType = "ERROR";
                            data.message = "Response Received";
                            systemLogs(data)
                                .catch((error) => {
                                    logger.error("MsgId: ", data.msgId, ": Error: EOD Fetch Customer ID Cron ", "date:" + new Date(), error);
                                });
                        }
                        //var regExp = new RegExp("^(?!2[0-9][0-9])$");
                        return response && !regExp.test(response.statusCode);
                        //return (response && response.statusCode !== 200);
                    } //requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    var endingTime = new Date();
                    logger.info("MsgId: ", data.msgId, "; Response time from Customer ID Tokenize ID Vault API: ", (endingTime - startingTime) / 1000, " sec");
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Customer ID Tokenize ID Vault ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Customer ID Tokenize ID Vault ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
                        if (body && body.token) {
                            data.transactionToProcess.custId = body.token;
                            resolve(data);
                        } else {
                            data.transactionToProcess.custId = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.custId);
                            var errorMsg = { "status": "Fail", "error": { "code": "1075", "messages": "Invalid response from Customer ID Tokenize API. Saved the encrypted Customer ID" } };
                            //data.transactionToProcess.result = "Fail";
                            data.transactionToProcess.error = data.transactionToProcess.error ? (data.transactionToProcess.error + ", " + errorMsg.error.messages) : errorMsg.error.messages;
                            //data.body.realized = true;
                            resolve(data);
                        }
                    } else {
                        data.transactionToProcess.custId = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.custId);
                        var errorMsg = { "error": { "code": "1071", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };

                        // data.transactionToProcess.error = data.transactionToProcess.error?(data.transactionToProcess.error+", "+JSON.stringify(errorMsg)+ ". Saved the encrypted Customer ID"):(JSON.stringify(errorMsg)+ ". Saved the encrypted Customer ID");
                        if (typeof errorMsg.error.messages === "string") {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + errorMsg.error.messages + ", Saved the encrypted Customer ID" : errorMsg.error.messages + ", Saved the encrypted Customer ID";
                        } else {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted Customer ID" : JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted Customer ID";
                        }
                        //data.transactionToProcess.realized = true;
                        resolve(data);

                    }
                });
        } else {
            resolve(data);
        }
    });
};

function getTokenizedCardNo(data) {
    return new Promise((resolve, reject) => {
        if (data.transactionToProcess.cardNo && data.failed) {
            var body = {
                "naeUser": data.config.cardNaeUser,
                "user2": data.config.cardUser2,
                "value": data.transactionToProcess.cardNo,
                "tableName": data.config.cardNoTableName,
                "format": data.config.tokenizeFormat
            };
            var idVaultBody = {
                "naeUser": data.config.cardNaeUser,
                "user2": data.config.cardUser2,
                "value": aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.cardNo),
                "tableName": data.config.cardNoTableName,
                "format": data.config.tokenizeFormat
            };
            // idVaultBody.value = aes256.encrypt("encrypted734329sensitive3ndsm832", idVaultBody.value);
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Card No Tokenize ID Vault ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Card No Tokenize ID Vault ", "request received: " + new Date(), "request: ", idVaultBody);
            data.header = {
                "Content-Type": "application/json",
                "Authorization": "Basic " + data.esbAuth
            }
            var startingTime = new Date();
            requestRetry({
                    headers: data.header,
                    url: data.config.tokenizeUrl,
                    method: 'POST',
                    body: body,
                    json: true,
                    timeout: data.config.idVaultApiTimeout,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: function(err, response, body, options) {
                        var regExp = new RegExp("^(2[0-9][0-9])$");
                        if (response && !regExp.test(response.statusCode)) {
                            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Card No Tokenize ID Vault ", "Exception response received: " + new Date());
                            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Card No Tokenize ID Vault ", "Exception response received: " + new Date(), "response: ", body || err);

                            data.resMsg = JSON.stringify(body || err);
                            data.logType = "ERROR";
                            data.message = "Response Received";
                            systemLogs(data)
                                .catch((error) => {
                                    logger.error("MsgId: ", data.msgId, ": Error: EOD Fetch Customer ID Cron ", "date:" + new Date(), error);
                                });
                        }
                        //var regExp = new RegExp("^(?!2[0-9][0-9])$");
                        return response && !regExp.test(response.statusCode);
                        //return (response && response.statusCode !== 200);
                    } //requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    var endingTime = new Date();
                    logger.info("MsgId: ", data.msgId, "; Response time from Card No Tokenize ID Vault API: ", (endingTime - startingTime) / 1000, " sec");
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Card No Tokenize ID Vault ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Card No ID Tokenize ID Vault ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
                        if (body && body.token) {
                            data.transactionToProcess.cardNo = body.token;
                            resolve(data);
                        } else {
                            data.transactionToProcess.cardNo = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.cardNo);
                            var errorMsg = { "status": "Fail", "error": { "code": "1074", "messages": "Invalid response from Card No Tokenize API. Saved the encrypted Card No" } };
                            //data.transactionToProcess.result = "Fail";
                            data.transactionToProcess.error = data.transactionToProcess.error ? (data.transactionToProcess.error + ", " + errorMsg.error.messages) : errorMsg.error.messages;
                            //data.transactionToProcess.realized = true;
                            resolve(data);
                        }
                    } else {
                        data.transactionToProcess.cardNo = aes256.encrypt("encrypted734329sensitive3ndsm832", data.transactionToProcess.cardNo);
                        var errorMsg = { "error": { "code": "1071", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };

                        // data.transactionToProcess.error = data.transactionToProcess.error?(data.transactionToProcess.error+", "+JSON.stringify(errorMsg)+ ". Saved the encrypted Card No"):(JSON.stringify(errorMsg)+ ". Saved the encrypted Card No");
                        if (typeof errorMsg.error.messages === "string") {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + errorMsg.error.messages + ", Saved the encrypted Card No" : errorMsg.error.messages + ", Saved the encrypted Card No";
                        } else {
                            data.transactionToProcess.error = data.transactionToProcess.error ? data.transactionToProcess.error + ", " + JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted Card No" : JSON.stringify(errorMsg.error.messages) + ", Saved the encrypted Card No";
                        }
                        //data.transactionToProcess.realized = true;
                        resolve(data);

                    }
                });
        } else {
            resolve(data);
        }
    });
};

function addFailedTransaction(data) {
    return new Promise((resolve, reject) => {
        console.log("count:  success: ", data.successfulCount, " failed: ", data.failedCount)
        if (data.failed && data.transactionToProcess.error) {
            // logger.debug("MsgId: ", data.msgId,": FCC Transaction Cron: Transaction processed: ", "date:" + new Date() ,data.transactionToProcess);
            var txnDateTime = data.transactionToProcess.transactionDateTime;
            var bookedDate = data.transactionToProcess.bookedDate;
            data.transactionToProcess.transactionDateTime = moment(txnDateTime).format('YYYY-MM-DD');
            data.transactionToProcess.bookedDate = moment(bookedDate).format('YYYY-MM-DD');
            var failedTransactions = data.failedTransactions;
            failedTransactions.push(data.transactionToProcess);
            data.failedTransactions = failedTransactions;
            logger.debug("MsgId: ", data.msgId, ": EOD FCC Transaction Cron: Transaction processed: ", "date:" + new Date(), data.transactionToProcess);
            resolve(data);
        } else {
            // var maskObject = data.transactionToProcess;

            // const mask = obj => {
            //     const masked = {};
            //     for (let key in maskObject) 
            //         if((key === "pan")) {
            //             masked[key] = aes256.encrypt("encrypted734329sensitive3ndsm832", maskObject[key]);
            //         } else if((key === "aadhaarNo")) {
            //             masked[key] = aes256.encrypt("encrypted734329sensitive3ndsm832", maskObject[key]);
            //         } else if((key === "cardNo")) {
            //             masked[key] = aes256.encrypt("encrypted734329sensitive3ndsm832", maskObject[key]);
            //         } else if((key === "custId")) {
            //             masked[key] = aes256.encrypt("encrypted734329sensitive3ndsm832", maskObject[key]);
            //         } else {
            //             masked[key] = maskObject[key];
            //         }
            //     return masked;
            // };
            // console.log("maskkkkkk: ", maskObject)
            // var maskedObject = mask(maskObject);
            var txnDateTime = data.transactionToProcess.transactionDateTime;
            var bookedDate = data.transactionToProcess.bookedDate;
            data.transactionToProcess.transactionDateTime = moment(txnDateTime).format('YYYY-MM-DD');
            data.transactionToProcess.bookedDate = moment(bookedDate).format('YYYY-MM-DD');
            logger.debug("MsgId: ", data.msgId, ": EOD FCC Transaction Cron: Successful Transaction processed: ", "date:" + new Date(), data.transactionToProcess);
            resolve(data);
        }

    });
};

function updateTimestamp(data) {
    return new Promise((resolve, reject) => {
        if (data.fromTime) {
            logger.debug("MsgId: ", data.msgId, "date:" + new Date() + "messages: Timestamp of EOD FCC Transaction unchanged");
            var errorMsg = { "error": { "messages": "Timestamp of EOD FCC Transaction unchanged" } };
            reject(errorMsg);
        } else {
            var body = {
                "cronSchedule": {
                    "eodFccTransaction": {
                        "lastRecordProcessedTime": data.processedTime,
                        "running": false
                    }
                }
            };
            //console.log("body: ", body)
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Timestamp ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Timestamp ", "request received: " + new Date(), "request: ", body);
            requestRetry({
                    headers: data.headers,
                    url: data.config.sandboxUrl + data.config.cronScheduleUrl + '/' + data.cronScheduleId,
                    method: 'PUT',
                    body: body,
                    json: true,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Timestamp ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Timestamp ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && response.statusCode == 200) {
                        logger.debug("MsgId: ", data.msgId, "date:" + new Date() + "messages: Timestamp for EOD FCC Transaction updated");
                        resolve(data);
                    } else {
                        var errorMsg = { "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                        logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                        data.fccSyncErrorMessage = "Error from ODP while updating Last Record Procossed Time in Cron Schedule";
                        data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                        data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                        sendEmail(data)
                            .catch((error) => {
                                console.log(error);
                            });
                        reject(errorMsg);
                    }
                });
        }
    });
};

function createFile(data) {
    return new Promise((resolve, reject) => {
        if (data.failedCount === 0) {
            resolve(data);
        } else {
            //console.log("Successful Count: ", data.successfulCount, " Failed Count: ", data.failedCount);
            //console.log("Data to be added in file: ", data.failedTransactions);
            var jsonData = data.failedTransactions;
            var xls = json2xls(jsonData);
            var datetime = moment(new Date()).format('YYYY-MM-DDHH-mm-ss');
            fs.writeFileSync('./api/controllers/FCCFailTransactions-' + datetime + '.xlsx', xls, 'binary');
            var formData = {
                file: fs.createReadStream(__dirname + '/FCCFailTransactions-' + datetime + '.xlsx'),
            };
            var filePath = __dirname + '/FCCFailTransactions-' + datetime + '.xlsx';
            // if (fs.existsSync(filePath)) {
            //     //file exists
            //     fs.unlinkSync(filePath);
            // }
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Upload File ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Upload File ", "request received: " + new Date(), "request: ", formData);
            requestRetry({
                    headers: data.headers,
                    url: data.config.sandboxUrl + data.config.fccTransactionsUrl + '/file/upload',
                    method: 'POST',
                    formData: formData,
                    json: true,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Upload File ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Upload File ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && response.statusCode == 200) {
                        data.fileData = body;
                        if (fs.existsSync(filePath)) {
                            //file exists
                            fs.unlinkSync(filePath);
                        }
                        logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: ", "File Uploaded: " + new Date(), "response: ", body);
                        logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: ", "File Uploaded: " + new Date());
                        resolve(data);
                    } else {
                        if (fs.existsSync(filePath)) {
                            //file exists
                            fs.unlinkSync(filePath);
                        }
                        var errorMsg = { "error": { "code": "1004", "functionName": "File Upload", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                        logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                        data.fccSyncErrorMessage = "Error from ODP while uploading Failed Records for process running from " + data.startTime + " to " + data.endTime + " in FCC Fail Transaction";
                        data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                        data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                        sendEmail(data)
                            .catch((error) => {
                                console.log(error);
                            });
                        reject(errorMsg);
                    }
                });
        }
    });
};

function updateFccTransactions(data) {
    return new Promise((resolve, reject) => {
        var body = {
            fromTime: data.startTime,
            toTime: data.endTime,
            file: (data.failedCount === 0) ? null : data.fileData,
            schedulerId: data.schedulerId,
            count: {
                numberOfSuccessfulRecords: data.successfulCount,
                numberOfFailedRecords: data.failedCount
                //failedTransaction: data.failedTransactions
            }
        }
        //console.log("body: ", body)
        logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Add in FCC Fail Transactions ", "request received: " + new Date());
        logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Add in FCC Fail Transactions ", "request received: " + new Date(), "request: ", body);
        requestRetry({
                headers: data.headers,
                url: data.config.sandboxUrl + data.config.fccTransactionsUrl,
                method: 'POST',
                body: body,
                json: true,
                maxAttempts: data.maxAttempts, // (default) try 5 times
                retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
            },
            function(error, response, body) {
                logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Add in FCC Fail Transactions ", "response received: " + new Date());
                logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Add in FCC Fail Transactions ", "response received: " + new Date(), "response: ", (body || error));
                if (!error && response.statusCode == 200) {
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: ", "request processed: " + new Date(), "response: ", body);
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: ", "request processed: " + new Date());
                    resolve(data);
                } else {
                    var errorMsg = { "error": { "code": "1004", "functionName": "Update FCC", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                    logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                    data.fccSyncErrorMessage = "Error from ODP while adding Successful and Failed count for process running from " + data.startTime + " to " + data.endTime + " in FCC Fail Transaction";
                    data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                    data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                    sendEmail(data)
                        .catch((error) => {
                            console.log(error);
                        });
                    reject(errorMsg);
                }
            });
    });
};

function updateToNotRunning(data) {
    return new Promise((resolve, reject) => {
        if (data.fromTime) {
            logger.debug("MsgId: ", data.msgId, "date:" + new Date() + "messages: Running status of EOD FCC Transaction unchanged");
            var errorMsg = { "error": { "messages": "Running status of EOD FCC Transaction unchanged" } };
            reject(errorMsg);
        } else {
            var body = {
                "cronSchedule": {
                    "eodFccTransaction": {
                        "running": false
                    }
                }
            };
            //console.log("body: ", body)
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Cron Schedule ", "request received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Cron Schedule ", "request received: " + new Date(), "request: ", body);
            requestRetry({
                    headers: data.headers,
                    url: data.config.sandboxUrl + data.config.cronScheduleUrl + '/' + data.cronScheduleId,
                    method: 'PUT',
                    body: body,
                    json: true,
                    maxAttempts: data.maxAttempts, // (default) try 5 times
                    retryDelay: data.retryDelay, // (default) wait for 5s before trying again
                    retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
                },
                function(error, response, body) {
                    logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Cron Schedule ", "response received: " + new Date());
                    logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Update Cron Schedule ", "response received: " + new Date(), "response: ", (body || error));
                    if (!error && response.statusCode == 200) {
                        logger.debug("MsgId: ", data.msgId, "date:" + new Date() + "messages: Status for EOD FCC Transaction updated");
                        resolve(data);
                        // data.job.start();
                        // data.cb(true);
                    } else {
                        var errorMsg = { "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                        logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
                        data.fccSyncErrorMessage = "Error from ODP while updating process completion for process running from " + data.startTime + " to " + data.endTime + " in Cron Schedule";
                        data.fccSyncError = errorMsg.error.messages.message ? errorMsg.error.messages.message : errorMsg.error.messages;
                        data.fccSyncTemplate = data.config.fccSyncErrorTemplate;
                        sendEmail(data)
                            .catch((error) => {
                                console.log(error);
                            });
                        reject(errorMsg);
                    }
                });
        }
    });
};

function systemLogs(data) {
    return new Promise((resolve, reject) => {
        //data.logHeader = data.headers;
        //delete data.logHeader.Authorization;
        data.logHeader = Object.keys(data.headers).reduce((object, key) => {
            if (key.toLowerCase() !== "authorization") {
                object[key] = data.headers[key]
            }
            return object
        }, {});
        var body = {
            "msgId": "" + data.msgId + "",
            "converstnId": moment(new Date()).format('DD-MMM-YYYY'),
            "serviceName": "EOD FCC Sync Scheduler",
            "serviceId": data.schedulerId ? data.schedulerId : "23",
            "headers": JSON.stringify(data.logHeader),
            "message": data.message,
            "body": data.resMsg,
            "logType": data.logType
        };

        // requestRetry({
        //         headers: data.headers,
        //         url: data.config.sandboxUrl + data.config.lrsApplicationLogs,
        //         method: 'POST',
        //         body: body,
        //         json: true,
        //         maxAttempts: data.maxAttempts, // (default) try 5 times
        //         retryDelay: data.retryDelay, // (default) wait for 5s before trying again
        //         retryStrategy: requestRetry.RetryStrategies.HTTPOrNetworkError
        //     },
        //     function(error, response, body) {
        //         if (!error && response.statusCode == 200) {
        //             resolve(data);
        //         } else {
        //             var errorMsg = { "status": "Fail", "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };

        //             logger.error("MsgId: ", data.msgId, ": Error: EOD FCC Transaction Cron ", "date:" + new Date(), errorMsg);
        //             reject(errorMsg);

        //         }
        //     });
        stanProducer.publish(JSON.stringify(body), data.config.applicationLogsQueue);
    });
};

function createFccSuccessFile(tempdata) {
    return new Promise((resolve, reject) => {
        console.log("Successful transactions count ", tempdata.completedTransactions.length);
        if (tempdata.completedTransactions.length !== 0) {
            var jsonObj = tempdata.completedTransactions;
            var excel = json2xls(jsonObj);
            var datetime = moment(new Date()).format('YYYY-MM-DDHH-mm-ss');
            fs.writeFileSync('FCCCompletedTransactions-' + datetime + '.xlsx', excel, 'binary');
            var file = 'FCCCompletedTransactions-' + datetime + '.xlsx';
            var fileData = fs.readFileSync(file);
            var zip = new JSZip();
            zip.file(file, fileData);
            var data = zip.generate({ base64: false, compression: 'DEFLATE' });
            fs.writeFileSync('FCCCompletedTransactions-' + datetime + '.zip', data, 'binary');
            var fileDataZip = fs.readFileSync('FCCCompletedTransactions-' + datetime + '.zip'),
                encodedData = new Buffer(fileDataZip).toString('base64');

            fs.unlinkSync(file);
            fs.unlinkSync('FCCCompletedTransactions-' + datetime + '.zip');

            tempdata.emailBody = {
                templateID: tempdata.config.fccSyncSuccessTemplate,
                params: [{ "FromDate": tempdata.startTime }, { "ToDate": tempdata.endTime }],
                channelName: "LRS",
                attachmentfileName: 'FCCCompletedTransactions-' + datetime + '.zip',
                encodedFileData: encodedData,
            };
            resolve(tempdata);
        } else {
            resolve(tempdata);
        }
    });
}

function sendEmail(data) {
    return new Promise((resolve, reject) => {
        if (data.emailBody) {
            var body = data.emailBody;
        } else {
            var body = {
                templateID: data.fccSyncTemplate,
                params: [{ "FromDate": moment(new Date()).format("DD-MMM-YYYY HH:mm:ss") }, { "Message": data.fccSyncErrorMessage }, { "Error": data.fccSyncError }],
                channelName: "LRS"
            };
        }
        logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Send Email ", "request received: " + new Date());
        logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Send Email ", "request received: " + new Date(), "request: ", body);
        request({
            url: data.config.sendEmailUrl,
            method: 'POST',
            body: body,
            json: true
        }, function(error, response, body) {
            logger.info("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Send Email ", "response received: " + new Date());
            logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron: Send Email ", "response received: " + new Date(), "response: ", (body || error));
            if (!error && response.statusCode == 200) {
                logger.debug("MsgId: ", data.msgId, "; EOD FCC Transaction Cron ", "request processed: " + new Date(), "response: Email sent Successfully");
                resolve(data);

            } else {
                var errorMsg = { "error": { "code": "1004", "messages": (body ? (body.messages ? ((body.messages && !body.code) ? body.messages : body) : body) : error) } };
                logger.error("MsgId: ", data.msgId, "Error: EOD FCC Transaction Cron ", ": date: " + new Date(), errorMsg);
                reject(errorMsg);
                //console.log("Error occurred while sending Transaction Report");
            }
        });
    });
}