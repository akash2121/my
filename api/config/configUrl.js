'use strict';

var config = {
	"sandboxUrl": process.env.ODPURL,
	"lrsApiUrl": process.env.LRSAPIURL,
	"cronScheduleUrl": "/api/c/LRS/cronSchedule",
	"fccTransactionsUrl": "/api/c/LRS/fccFailTransaction",
	"tokenizeUrl": process.env.IDVAULT_TOKENIZE_URL,
	"aadhaarNaeUser": process.env.IDVAULT_AADHAAR_NAEUSER,
	"aadhaarUser2": process.env.IDVAULT_AADHAAR_USER2,
	"panNaeUser": process.env.IDVAULT_PAN_NAEUSER,
	"panUser2": process.env.IDVAULT_PAN_USER2,
	"cardNaeUser": process.env.IDVAULT_CARD_NAEUSER,
	"cardUser2": process.env.IDVAULT_CARD_USER2,
	"custidNaeUser": process.env.IDVAULT_CUSTID_NAEUSER,
	"custidUser2": process.env.IDVAULT_CUSTID_USER2,
	"panTableName": process.env.IDVAULT_PAN_TABLENAME,
	"custIdTableName": process.env.IDVAULT_CUSTID_TABLENAME,
	"cardNoTableName": process.env.IDVAULT_CARDNO_TABLENAME,
	"aadhaarNoTableName": process.env.IDVAULT_AADHAARNO_TABLENAME,
	"tokenizeFormat": process.env.IDVAULT_TOKENIZE_FORMAT,
	"esbAuth": process.env.ESBAUTH,
	"lrsApplicationLogs": "/api/c/LRS/lrsApplicationLogs",
	"logLevel": process.env.LOG_LEVEL?(process.env.LOG_LEVEL).toUpperCase():"INFO",
	"applicationLogsQueue": process.env.APPLICATIONLOGS_QUEUE?process.env.APPLICATIONLOGS_QUEUE:"syslogs",
	"natsIp": process.env.NATS_IP?process.env.NATS_IP:"localhost",
	"natsPort": process.env.NATS_PORT?process.env.NATS_PORT:"4222",
	"fccSyncErrorTemplate": process.env.FCCSYNCERROR_TEMPLATE,
	"sendEmailUrl": process.env.SENDEMAILURL,
	"fccSyncSuccessTemplate": process.env.FCCSYNCSUCCESS_TEMPLATE,
	"idVaultApiTimeout": process.env.IDVAULT_API_TIMEOUT?parseInt(process.env.IDVAULT_API_TIMEOUT):1000
}

module.exports = {
	config
}