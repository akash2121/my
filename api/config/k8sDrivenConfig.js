var e = {
	omniDataPlatformUser: process.env.ODPUSER,
	omniDataPlatformPassword: process.env.ODPPWD,
	databaseUser: process.env.DBUSER,
	databasePassword: process.env.DBPWD,
	DBHOST: process.env.DBHOST,
	DBPORT: process.env.DBPORT,
	DBSERVICE_NAME: process.env.DBSERVICE_NAME
};

// function isK8sEnv(){
// 	return process.env.KUBERNETES_SERVICE_HOST && process.env.KUBERNETES_SERVICE_PORT;
// }

// if(!isK8sEnv()){
// 	// Set default values
// 	e.omniDataPlatformUser = process.env.ODPUSER;
// 	e.omniDataPlatformPassword = process.env.ODPPWD;
// 	e.databaseUser = process.env.DBUSER;
// 	e.databasePassword = process.env.DBPWD;
// } else {

// }

module.exports = e;