'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
module.exports = app; // for testing
const log4js = require('log4js');

log4js.configure({
    "appenders": {
        "out": { "type": "stdout" },
        "result": { "type": "file", "filename": "logs/log", "keepFileExt": true, "maxLogSize": 10485760, "backups": 5, "compress": true},
        "result-filter": { "type": "logLevelFilter", "appender": "result", "level": "debug", "maxLevel": "error" }
    },
    "categories": {
        "default": { "appenders": ["out", "result-filter"], "level": "debug" }
    }
});

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var fccTransactionCron = require('./api/controllers/fccTransactionCron.js');

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware

  fccTransactionCron.startCron();

  swaggerExpress.register(app);

  var port = process.env.PORT || 3000;
  var server = app.listen(port);
  server.timeout = process.env.SERVER_TIMEOUT?parseInt(process.env.SERVER_TIMEOUT):120000; // 120000 default
  console.log("Connected on port: ", port);

});
